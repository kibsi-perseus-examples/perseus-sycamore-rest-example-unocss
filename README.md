# perseus-sycamore-rest-example-unocss



## Clone the repository

First clone the repository as usual

```
git clone https://gitlab.com/kibsi-perseus-examples/perseus-sycamore-rest-example-unocss.git
```

## Prerequisites

You must have [node + npm](https://nodejs.org/) installed!  
You must also have the [perseus-cli](https://framesurge.sh/perseus/) installed  
Note that this project uses the version 0.4 of perseus which is currently in beta

## Prepare for run

After you checked out the project, navigate into the folder, that has the file 'package.json' in it.  
Run

```
npm install
```

to install all the npm packages required for this project.  
They are only used during the development phase.  
Then go ahead and run

```
perseus serve -w
```

to run the perseus test server.