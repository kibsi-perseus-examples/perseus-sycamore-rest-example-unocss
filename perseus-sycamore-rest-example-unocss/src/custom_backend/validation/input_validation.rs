
use ammonia::clean;
use chrono::{NaiveDate, Duration, Days, NaiveDateTime,Utc};
use rustrict::CensorStr;
use email_address::EmailAddress;

use crate::custom_backend::custom_errors::validation_error::ValidationError;

static SHORT_STRING_LENGTH: usize = 40;
static MID_STRING_LENGTH: usize = 320;
// This is the max length of an email, we take it as our mid length
static _EMAIL_STRING_LENGTH: usize = 320;
static _LONG_STRING_LENGTH: usize = 4000;
static _INSANE_STRING_LENGTH: usize = 16000;

static MIN_PASSWORD_LENGTH: usize = 8;

static _MINIMUM_AGE: u32 = 16;

pub fn _validate_expiration(compare_time: NaiveDateTime, expiration_time: Duration) -> Result<NaiveDateTime, ValidationError> {
    let now = Utc::now().naive_local();
    
    let token_valid_until = compare_time + expiration_time;
    if now > token_valid_until {
        return Err(ValidationError::ChronoExpiredErrror());
    }
    Ok(compare_time)
}

pub fn _validate_minimum_age_str(date_of_birth_str: &str)-> Result<NaiveDate, ValidationError> {
    let date_of_birth = NaiveDate::parse_from_str(date_of_birth_str, "%Y-%m-%d")?;
    _validate_minimum_age(date_of_birth)
}

pub fn _validate_minimum_age(date_of_birth: NaiveDate) -> Result<NaiveDate, ValidationError> {
    // This is not very accurate, we would need the time zone of the user, but that is complicated and not reliable
    let now = Utc::now().naive_local().date();
    // Because we do not know the exact timezone of the user, we make hin one day younger and calculate with that instead
    let date_of_birth_and_one = date_of_birth.checked_add_days(Days::new(1));
    if let Some(date_of_birth_and_one) = date_of_birth_and_one {
        
        let years_old_option = now.years_since(date_of_birth_and_one);
        
        match years_old_option {
            Some(years_old) => if years_old < _MINIMUM_AGE {
                Err(ValidationError::BelowMinimumAge{is_age: years_old, minimum_age: _MINIMUM_AGE})
            } else {
                Ok(date_of_birth)
            },
            None => Err(ValidationError::GenericDateError())
        }
    } else {
        Err(ValidationError::GenericDateError())
    }
}

pub fn validate_password_string(unvalidated: &str) -> Result<&str, ValidationError> {
    validate_string_length(unvalidated,MIN_PASSWORD_LENGTH,MID_STRING_LENGTH,true, true)
    // TODO check for multiple  categories of letters
}

pub fn validate_short_string(unvalidated: &str, required: bool, email_allowed: bool) -> Result<&str, ValidationError> {
    validate_string_length(unvalidated, if required {1} else {0}, SHORT_STRING_LENGTH, false, email_allowed)
}

pub fn _validate_mid_string(unvalidated: &str, required: bool, email_allowed: bool) -> Result<&str, ValidationError> {
    validate_string_length(unvalidated, if required {1} else {0}, MID_STRING_LENGTH, false, email_allowed)
}

pub fn validate_not_email_string(unvalidated: &str) -> Result<&str, ValidationError> {
    let email_ok = EmailAddress::is_valid(unvalidated);
    if email_ok {
        Err(ValidationError::EmailNotAllowed())
    } else {
        Ok(unvalidated)
    }
}

pub fn validate_email_string(unvalidated: &str) -> Result<&str, ValidationError> {
    let email_ok = EmailAddress::is_valid(unvalidated);
    if email_ok {
        Ok(unvalidated)
    } else {
        Err(ValidationError::EmailRejected{email: unvalidated.to_owned()})
    }
}

pub fn _validate_long_string(unvalidated: &str, required: bool, email_allowed: bool) -> Result<&str, ValidationError> {
    validate_string_length(unvalidated, if required {1} else {0}, _LONG_STRING_LENGTH, false, email_allowed)
}

pub fn _validate_insane_string(unvalidated: &str, required: bool, email_allowed: bool) -> Result<&str, ValidationError> {
    validate_string_length(unvalidated, if required {1} else {0}, _INSANE_STRING_LENGTH, false, email_allowed)
}

pub fn validate_string_length(unvalidated: &str, min_length: usize, max_length: usize, only_check_length: bool, email_allowed: bool) -> Result<&str, ValidationError> {
    let unvalidated_length = unvalidated.len();
    if min_length == 0 && unvalidated_length == 0 {
        return Ok(unvalidated)
    }
    if unvalidated_length > max_length {
        return Err(ValidationError::TooLong { is: unvalidated_length, max: SHORT_STRING_LENGTH });
    } else if unvalidated_length < min_length {
        return Err(ValidationError::TooShort { is: unvalidated_length, min: SHORT_STRING_LENGTH });
    }
    if only_check_length {
        validate_html_sanitize(unvalidated)?;
        validate_censor_input(unvalidated)?;
    }
    if !email_allowed {
        validate_not_email_string(unvalidated)?;
    }
    Ok(unvalidated)
}

fn validate_html_sanitize(unvalidated: &str) -> Result<&str, ValidationError> {
    // maybe there is a better way to do this?
    let cleaned = clean(unvalidated);
    if cleaned.eq(unvalidated) {
        Ok(unvalidated)
    } else {
        Err(ValidationError::Cleaned { original: unvalidated.to_owned(), cleaned })
    }
}

fn validate_censor_input(unvalidated: &str) -> Result<&str, ValidationError> {
    // We do not allow profanity, prevent it if we detect it
    let inapproprate = unvalidated.is_inappropriate();
    if inapproprate {
        Err(ValidationError::Inapproprate{original: unvalidated.to_owned()})
    } else {
        Ok(unvalidated)
    }
}