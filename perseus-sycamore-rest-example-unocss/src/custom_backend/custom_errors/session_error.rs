use thiserror::Error;

#[derive(Error, Debug)]
pub enum SessionError {

    #[error("Can not access the session")]
    SessionAccessError(),

    #[error("Can not access the DB pool to get the persisted Session")]
    DbPoolAccessError(),

    #[error("The session token is unavailable")]
    SessionTokenUnavailable(),

    #[error("The session token is expired")]
    SessionTokenExpired(),
}