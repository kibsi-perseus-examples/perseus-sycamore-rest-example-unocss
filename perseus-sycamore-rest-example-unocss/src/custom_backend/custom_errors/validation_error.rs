use thiserror::Error;

#[derive(Error, Debug)]
pub enum ValidationError {

    #[error("Size is too short {is:?}, {min:?}")]
    TooShort{is: usize, min: usize},

    #[error("Size is too long {is:?}, {max:?}")]
    TooLong{is: usize, max: usize},

    #[error("The input is not clean. original: {original:?}, cleaned: {cleaned:?}")]
    Cleaned{original: String, cleaned: String},

    // We do not allow profanity
    #[error("The input is inapproprate. original: {original:?}")]
    Inapproprate{original: String},

    #[error("The email does not conform to the RFC 5322 standard. email: {email:?}")]
    EmailRejected{email: String},

    #[error("An email address is not allowed for this field")]
    EmailNotAllowed(),

    #[error("Not old enough. {is_age:?}, {minimum_age:?}")]
    BelowMinimumAge{ is_age: u32, minimum_age: u32},

    #[error("A generic Date error occured, life goes on")]
    GenericDateError(),

    #[error("chrono could not parse")]
    ChronoParseError(chrono::format::ParseError),

    #[error("It is expired")]
    ChronoExpiredErrror(),

    #[error("The provided token is not base62 formatted or is otherwise invalid")]
    InvalidTokenError(),

    #[error("The password hash does not exist. This should not happen.")]
    PasswordHashDoesNotExist(),
}

impl From<chrono::format::ParseError> for ValidationError {
    fn from(value: chrono::format::ParseError) -> Self {
        Self::ChronoParseError(value)
    }
}
