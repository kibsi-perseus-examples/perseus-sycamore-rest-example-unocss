use std::ops::{Deref,DerefMut};
use crate::custom_backend::{custom_errors::validation_error::ValidationError, custom_errors::session_error::SessionError};
use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
};
use tracing::error;

pub struct AppError(anyhow::Error);

impl AppError {
    pub fn downcast_into_response(&self) -> impl IntoResponse {
        // If it is an input validation error, we want the users to actually know what they did wrong
        if let Some(err) = self.0.downcast_ref::<ValidationError>() {
            // Actually this is probably still not a good idea, send a generic message and log the error instead
            // return (StatusCode::NOT_ACCEPTABLE, err.to_string())
            error!("Validation error occured: {}",err);
            return (StatusCode::NOT_ACCEPTABLE, format!("Invalid input"))
        }
        if let Some(err) = self.0.downcast_ref::<SessionError>() {
            match err {
                &SessionError::SessionTokenExpired() => {
                    error!("Session token expired: {}",err);
                    return (StatusCode::NETWORK_AUTHENTICATION_REQUIRED, format!("Session expired, please log in again"));
                },
                &SessionError::SessionTokenUnavailable() => {
                    error!("Session token not found: {}",err);
                    return (StatusCode::NETWORK_AUTHENTICATION_REQUIRED, format!("Could not find session, please log in again"));
                },
                _ => {
                    error!("Unspecific session error occured: {}",err);
                    return (StatusCode::INTERNAL_SERVER_ERROR, format!("A session error occured, please log in again"));
                }
            }
        }
        error!("Some internal error occured: {}", self.0);
        (StatusCode::INTERNAL_SERVER_ERROR, format!("Errors happened, do not worry"))
    }
}

impl<E> From<E> for AppError
where E: Into<anyhow::Error>{
    fn from(other: E) -> Self {
        Self(other.into())
    }
}

impl Deref for AppError {
    type Target = anyhow::Error;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for AppError {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        self.downcast_into_response().into_response()
    }
}
