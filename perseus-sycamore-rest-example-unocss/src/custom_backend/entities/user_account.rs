use struct_fragment::{StructFragment};
use serde::{Serialize, Deserialize};
use std::cmp::{PartialEq, Eq};

#[derive(Default, Debug, PartialEq, Eq, sqlx::Type ,StructFragment, Serialize, Deserialize)]
// DbUserAccountInsert is derived without the user_account_id column.
// This is easy for small structs to do manually, but if the structs get bigger, it is nice that it happens automatically
// This is neccessary, because the DB generates the id
#[fragment_ignore_list = "user_account_id"]
#[fragment_name = "DbUserAccountInsert"]
pub struct DbUserAccountFull {
    pub user_account_id: i64,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
}

impl From<DbUserAccountFull> for DbUserAccountInsert {
    fn from(full: DbUserAccountFull) -> Self {
        Self {
            first_name: full.first_name,
            last_name: full.last_name,
        }
    }
}
