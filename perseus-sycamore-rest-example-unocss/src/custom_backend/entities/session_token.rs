use serde::{Serialize, Deserialize};
use struct_fragment::{StructFragment};
use std::{cmp::{PartialEq, Eq}};

#[derive(Default, Debug, PartialEq, Eq, sqlx::Type, StructFragment, Serialize, Deserialize)]
#[fragment_ignore_list = "user_account_id_ref"]
#[fragment_name = "DbSessionTokenInsert"]
pub struct DbSessionTokenFull {
    pub user_account_id_ref: i64,
    pub session_token: Option<String>,
    pub creation_time: i64,
}

impl From<DbSessionTokenFull> for DbSessionTokenInsert {
    fn from(full: DbSessionTokenFull) -> Self {
        Self {
            session_token: full.session_token,
            creation_time: full.creation_time,
        }
    }
}