use serde::{Serialize, Deserialize};
use std::cmp::{PartialEq, Eq};
use struct_fragment::StructFragment;

#[derive(Default, Debug, PartialEq, Eq, Serialize, Deserialize, sqlx::Type, StructFragment)]
#[fragment_ignore_list = "user_account_id_ref"]
#[fragment_name = "DbUserLoginDataInsert"]
pub struct DbUserLoginDataFull {
    pub user_account_id_ref: i64,
    pub username: Option<String>,
    pub password_encrypted: Option<String>,
    pub email: Option<String>,
    pub account_activated: bool,
}

impl From<DbUserLoginDataFull> for DbUserLoginDataInsert {
    fn from(full: DbUserLoginDataFull) -> Self {
        Self {
            username: full.username,
            password_encrypted: full.password_encrypted,
            email: full.email,
            account_activated: full.account_activated,
        }
    }
}