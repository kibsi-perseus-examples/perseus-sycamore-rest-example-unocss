use sqlx::{self, Sqlite, Transaction};
use anyhow::Result;

use crate::custom_backend::entities::{
    user_login_data::{DbUserLoginDataFull, DbUserLoginDataInsert},
    user_account::DbUserAccountFull};


#[derive(Debug)]
pub struct UserLoginDataDao {
}

impl UserLoginDataDao {

    pub async fn _find_by_user_id(tx: &mut Transaction<'_, Sqlite>, user_id: i64) -> Result<DbUserLoginDataFull> {
        let user_login_full = sqlx::query_as!(DbUserLoginDataFull,
r#"
SELECT
  *
FROM USER_LOGIN_DATA
WHERE
  user_account_id_ref = $1
Limit 1
"#,
        user_id).fetch_one(&mut *tx).await?;
        Ok(user_login_full)
    }

    pub async fn find_by_username(tx: &mut Transaction<'_, Sqlite>, username: &str) -> Result<DbUserLoginDataFull> {
        let username = Some(username);
        let user_login_data_full = sqlx::query_as!(DbUserLoginDataFull,
r#"
SELECT
  *
FROM USER_LOGIN_DATA
WHERE
  USERNAME = $1
Limit 1
"#,
        username
    ).fetch_one(&mut *tx).await?;
        Ok(user_login_data_full.into())
    }

    pub async fn insert(tx: &mut Transaction<'_, Sqlite>,user: &DbUserAccountFull ,login_data: &DbUserLoginDataInsert) -> Result<()> {
        sqlx::query!(
r#"
INSERT INTO USER_LOGIN_DATA (
  user_account_id_ref
, username
, password_encrypted
, email
, account_activated
)
VALUES ( $1, $2, $3, $4, $5 )
"#,
            user.user_account_id,
            login_data.username,
            login_data.password_encrypted,
            login_data.email,
            login_data.account_activated
                ).execute(&mut *tx).await?;
        Ok(())
    }

    pub async fn _update(tx: &mut Transaction<'_, Sqlite>, login_data: DbUserLoginDataFull) -> Result<()> {
        sqlx::query!(
r#"
UPDATE USER_LOGIN_DATA
SET
  username = $2
, password_encrypted = $3
, email = $4
, account_activated = $5
WHERE
  user_account_id_ref = $1
"#,
            // Identifiers first
            login_data.user_account_id_ref,
            // New data
            login_data.username,
            login_data.password_encrypted,
            login_data.email,
            login_data.account_activated,
        ).execute(&mut *tx).await?;
        Ok(())
    }
   
}
