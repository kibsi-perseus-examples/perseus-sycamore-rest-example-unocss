use crate::custom_backend::entities::user_account::{DbUserAccountFull, DbUserAccountInsert};
use sqlx::{self, Transaction, Sqlite};
use anyhow::Result;

#[derive(Debug)]
pub struct UserAccountDao {
}


impl UserAccountDao {

    pub async fn create(tx: &mut Transaction<'_, Sqlite>, user: &DbUserAccountInsert) -> Result<DbUserAccountFull> {
        let user_account_full = sqlx::query_as!(DbUserAccountFull,
r#"
INSERT INTO USER_ACCOUNT (
  first_name
, last_name
)
VALUES ( $1, $2 )
RETURNING *
;"#,
            user.first_name,
            user.last_name
            ).fetch_one(&mut *tx).await?;
        Ok(user_account_full.into())
        //    users.into_iter().next().context("No next")
    }

    pub async fn _find_by_id(tx: &mut Transaction<'_, Sqlite>, user_id: i64) -> Result<DbUserAccountFull> {
        let user_account_full = sqlx::query_as!(DbUserAccountFull,
r#"
SELECT
  *
FROM USER_ACCOUNT
WHERE
  user_account_id = $1
;"#,
            user_id
            ).fetch_one(&mut *tx).await?;
        Ok(user_account_full)
            //).fetch_all(&mut *tx).await?;
        //users.into_iter().next().context("No next")
    }

}