use crate::custom_backend::{entities::session_token::DbSessionTokenFull};
use chrono::Utc;
use rand::distributions::{Alphanumeric, DistString};
use sqlx::{self, Sqlite, Transaction};


#[derive(Debug)]
pub struct SessionTokenDao {
}

impl SessionTokenDao {

    pub async fn generate_session_token(tx: &mut Transaction<'_, Sqlite>, user_id: i64) -> anyhow::Result<String> {

        let session_token_string = Alphanumeric.sample_string(&mut rand::thread_rng(), 16);
        let session_token = session_token_string.clone();
        let timestamp = Utc::now();
        let timestamp = timestamp.timestamp_millis();
        sqlx::query!(
r#"
insert into SESSION_TOKEN (
  user_account_id_ref
, session_token
, creation_time
)
VALUES ($1, $2, $3)
;"#
            , user_id
            , session_token
            , timestamp
        ).execute(&mut *tx).await?; // TODO check if fetch_one works
        Ok(session_token_string)
    }

    pub async fn delete_session_token(tx: &mut Transaction<'_, Sqlite>, session_token: &str) -> anyhow::Result<()> {
      sqlx::query!(
r#"
delete from SESSION_TOKEN
where session_token =$1
;"#, session_token).execute(&mut *tx).await?;
        Ok(())
    }

    pub async fn find_by_session_token(tx: &mut Transaction<'_, Sqlite>, session_token: &str) -> anyhow::Result<DbSessionTokenFull> {
        let session_token_full = sqlx::query_as!(DbSessionTokenFull,
r#"
select
  *
from SESSION_TOKEN
where
  session_token =$1
;"#
            ,session_token).fetch_one(&mut *tx).await?;
        Ok(session_token_full.into())
    }



}