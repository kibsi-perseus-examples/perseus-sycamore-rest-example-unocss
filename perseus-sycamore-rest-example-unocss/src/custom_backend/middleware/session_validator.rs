use std::sync::Arc;

use anyhow::Result;
use axum::{middleware::Next, response::Response, body::Bytes};
use axum_sessions::{extractors::ReadableSession, SessionHandle};
use chrono::{Utc, Duration, NaiveDateTime};
use perseus::http::{Request, StatusCode, Error};
use sqlx::{Sqlite, Pool};

use crate::custom_backend::{
    daos::session_token_dao::SessionTokenDao,
    lazy_mans_app_error::AppError,
    custom_errors::{session_error::SessionError,
    validation_error::ValidationError},
};


pub async fn confirm_session_valid<B>(
    request: Request<B>,
    next: Next<B>,
) -> Result<Response,AppError>{

    { // Put it into a scope so that request is free to be handed over
        let session_handle = request.extensions().get::<SessionHandle>().ok_or(SessionError::SessionAccessError())?;
        //let session_handle = match session_handle
        let pool = request.extensions().get::<Arc<Pool<Sqlite>>>().ok_or(SessionError::DbPoolAccessError())?;
        let session = session_handle.read().await;
        let session_token = session.get::<String>("SESSION_TOKEN").ok_or(SessionError::SessionTokenUnavailable())?;
        let mut tx = pool.begin().await?;

        let session_token = SessionTokenDao::find_by_session_token(&mut tx, &session_token).await?;
        let creation_time = session_token.creation_time;
        let creation_time = NaiveDateTime::from_timestamp_millis(creation_time).ok_or(ValidationError::GenericDateError())?;
        let now = Utc::now().naive_utc();
        //TODO: Increase the time according to your needs.
        // The initial 10 Minutes are only good for manual testing.
        let token_vaild_duration = Duration::minutes(10);
        let token_expiry_time = creation_time + token_vaild_duration;
        if token_expiry_time < now {
            return Err(SessionError::SessionTokenExpired().into());
        }
    }

    let response = next.run(request).await;

    Ok(response)
}