use crate::custom_backend::{handlers::account_handler, middleware::session_validator};
use std::{net::SocketAddr, sync::Arc};
use sqlx::sqlite::SqlitePoolOptions;
use tracing::Level;
use axum::{
    Router,
    routing::{get, post}, Extension, middleware,
};
use axum_sessions::{async_session::{MemoryStore}, SessionLayer, SameSite};
use perseus::{stores::MutableStore, i18n::TranslationsManager, turbine::Turbine, server::ServerOptions};
use rand::{thread_rng, Rng};
use tower_http::trace::{self, TraceLayer};


pub async fn server<M: MutableStore + 'static, T: TranslationsManager + 'static>(
    turbine: &'static Turbine<M, T>,
    opts: ServerOptions,
    (host, port): (String, u16),
) {
    let addr: SocketAddr = format!("{}:{}", host, port)
        .parse()
        .expect("Invalid address provided to bind to.");
    let app = get_router(turbine, opts).await;


    axum::Server::bind(&addr)
        .serve(tower::make::Shared::new(app))
        .await
        .unwrap();
}

pub async fn get_router<M: MutableStore + 'static, T: TranslationsManager + 'static>(
    turbine: &'static Turbine<M, T>,
    opts: ServerOptions,
) -> Router {
    let mut random = thread_rng();
    let mut secret = [0u8; 128];
    random.fill(&mut secret);
    let session_store = MemoryStore::new();
    let session_layer = SessionLayer::new(session_store, &secret)
    .with_secure(true)
    .with_same_site_policy(SameSite::Strict)
    ;

    let pool = SqlitePoolOptions::new()
    .max_connections(5)
    .connect("sqlite://dist/mutable/sqlite.db").await.unwrap();

    let row: (i64,) = sqlx::query_as("SELECT $1")
    .bind(150_i64)
    .fetch_one(&pool).await.unwrap();
    assert_eq!(row.0, 150);
    print!("{}", row.0);

    let arc_pool = Arc::new(pool);

    let api_routes = Router::new()
    .route("/protected", get(check_backend_session_protection_works))
    .layer(middleware::from_fn(session_validator::confirm_session_valid)) // Everything above here must have a vlaid session!
    .route("/register", post(account_handler::handle_register))
    .route("/login", post(account_handler::handle_login))
    .route("/logout", post(account_handler::handle_logout))
    .route("/status",get(check_backend_router_works))
    .layer(session_layer)
    .layer(Extension(arc_pool.clone()))
    ;

    let combined_router = perseus_axum::get_router(turbine, opts).await
    .nest("/api", api_routes)
    .layer(
        TraceLayer::new_for_http()
            .make_span_with(trace::DefaultMakeSpan::new()
                .level(Level::INFO))
            .on_response(trace::DefaultOnResponse::new()
                .level(Level::INFO)),
    )
    ;

    combined_router
}


async fn check_backend_router_works() -> &'static str {
    "Custom backend route is working!"
}

async fn check_backend_session_protection_works() -> &'static str {
    "You are currently logged in and therefore you can see this text!"
}