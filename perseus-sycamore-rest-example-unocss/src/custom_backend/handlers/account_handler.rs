use std::{sync::Arc};

use axum::{Extension, Json};
use axum_sessions::extractors::WritableSession;
use rand::rngs::OsRng;
use scrypt::{
    password_hash::{PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Scrypt,
};
use sqlx::SqlitePool;
use tracing::{debug, info};
use crate::{
    custom_backend::{
        validation::input_validation::{
            validate_short_string,
            validate_email_string,
            validate_password_string,
        },
        entities::{
            user_account::DbUserAccountInsert,
            user_login_data::DbUserLoginDataInsert,
        },
        daos::{
            user_account_dao::UserAccountDao,
            user_login_data_dao::UserLoginDataDao,
            session_token_dao::SessionTokenDao,
        },
        lazy_mans_app_error::AppError,
        custom_errors::{session_error::SessionError, validation_error::ValidationError},
    },
    request_types::account_requests::{RegisterRequest, RegisterResponse, LogoutRequest, LogoutResponse, LoginRequest, LoginResponse}, 
    
};

//#[axum_macros::debug_handler]
// https://rustc-dev-guide.rust-lang.org/tracing.html
//#[instrument(level = "debug")]
pub async fn handle_register(
    //mut session: WritableSession,
    arc_pool_ext: Extension<Arc<SqlitePool>>,
    Json(payload): Json<RegisterRequest>,
) -> anyhow::Result<Json<RegisterResponse>,AppError> {

    info!("Registering new user");
    // This tests are just some examples, but they are probably not complete.
    // For a real app we will have to critically think about what all needs to be checked.
    // Maybe there is a middleware, that we can put inbetween.
    let first_name = validate_short_string(&payload.first_name, true, false);
    let last_name = validate_short_string(&payload.last_name, true, false);
    let email = validate_email_string(&payload.email);
    let username = validate_short_string(&payload.username, true, false);
    let password = validate_password_string(&payload.password);
    
    if first_name.is_err()
    || last_name.is_err()
    || email.is_err()
    || username.is_err()
    || password.is_err() {
        return Ok(Json(RegisterResponse {
            firstname_ok: first_name.is_ok(),
            last_name_ok: last_name.is_ok(),
            email_ok: email.is_ok(),
            username_ok: username.is_ok(),
            password_ok: password.is_ok()
        }))
    }

    let first_name = first_name?.to_owned();
    let last_name = last_name?.to_owned();
    let email = email?.to_owned();
    let username = username?.to_owned();
    let password = password?.to_owned();

    let mut tx = arc_pool_ext.begin().await?;
    
    let user_account = DbUserAccountInsert {
        first_name: Some(first_name),
        last_name: Some(last_name),
    };

    let user_account = UserAccountDao::create(&mut tx, &user_account).await?;

    let salt = SaltString::generate(&mut OsRng);
    let password_hash = Scrypt.hash_password(password.as_bytes(), &salt)?;
    let password_hash = password_hash.to_string();

    UserLoginDataDao::insert(&mut tx, &user_account, &DbUserLoginDataInsert{
        username: Some(username),
        password_encrypted: Some(password_hash),
        email: Some(email.clone()),
        account_activated: true // TODO: normally the account is not activated at this point. The user would have to confirm the email
    }).await?;
    // TODO: Normally we would need to create a token that is sent to the email of the user.
    // But this is out of the scope of this example
    // Instead we just accept the new user account
    // But it would look something like the following lines
    // let account_activation_token = AccountActivationTokenDao::generate_account_activation_token_v2(&mut tx, user_account.user_account_id).await?;
    // send_account_activation_email(&email, &account_activation_token);
    
    tx.commit().await?; // If everything is ok, commit. Maybe we can put this into some middleware in the future?
    
    Ok(Json(RegisterResponse {
        firstname_ok: true,
        last_name_ok: true,
        email_ok: true,
        username_ok: true,
        password_ok: true
    }))
}

pub async fn handle_logout(
    mut session: WritableSession,
    arc_pool_ext: Extension<Arc<SqlitePool>>,
    Json(_payload): Json<LogoutRequest>,
) -> anyhow::Result<Json<LogoutResponse>,AppError> {
    let mut tx = arc_pool_ext.begin().await?;

    let session_token = session.get::<String>("SESSION_TOKEN").ok_or(SessionError::SessionTokenUnavailable());
    session.remove("SESSION_TOKEN");
    debug!("Removed SESSION_TOKEN");
    match session_token {
        Ok(session_token) => {
            let remove_result = SessionTokenDao::delete_session_token(&mut tx, &session_token).await;
            match remove_result {
                Ok(_) => {
                    debug!("Removed SESSION_TOKEN from DB");
                    ()
                },
                Err(err) => {
                    debug!("SQLX error removing SESSION_TOKEN from DB: {err}");
                    ()
                },
            }
        }
        Err(err) => {
            debug!("Session error getting SESSION_TOKEN from session: {err}");
            ()
        }
    }
    debug!("Logged out!");
    Ok(Json(LogoutResponse{
        logged_out: true    
    }))
}

//#[axum::debug_handler]
pub async fn handle_login(
    mut session: WritableSession,
    arc_pool_ext: Extension<Arc<SqlitePool>>,
    Json(payload): Json<LoginRequest>,
) -> anyhow::Result<Json<LoginResponse>,AppError> {
    let mut tx = arc_pool_ext.begin().await?;
    // it can either be a username or an email, but not both.
    // Because the username can not be changed to an email
    // And it is prevented to be an email at account creation
    
    let username = validate_short_string(&payload.username, true, false);

    let user_login_data = UserLoginDataDao::find_by_username(&mut tx, username?).await?;
    if !user_login_data.account_activated {
        return Ok(Json(LoginResponse{
            account_activated: false,
        }));
    }
    let password_encrypted = user_login_data.password_encrypted.ok_or(ValidationError::PasswordHashDoesNotExist())?;
    let parsed_hash = PasswordHash::new(&password_encrypted)?;
    Scrypt.verify_password(&payload.password.as_bytes(), &parsed_hash)?;
    
    let session_token = SessionTokenDao::generate_session_token(&mut tx, user_login_data.user_account_id_ref).await?;
    session.insert("SESSION_TOKEN", session_token)?;
    
    tx.commit().await?;
    Ok(Json(LoginResponse{
        account_activated: true,
    }))
}