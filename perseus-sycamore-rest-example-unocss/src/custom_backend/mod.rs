#![cfg(engine)]
// In this module we only have engine code. Note the '!' which marks everything as engine
// None of this code will go to the client. Non of it will go into the create wasm
pub mod custom_server;
// This is a quick and dirty solution.
// It is not idiomatic Rust for multiple reasons
// But it is ok for a quick start, we can switch to a fancy solution later
pub mod lazy_mans_app_error;
pub mod custom_errors;
pub mod daos;
pub mod entities;
pub mod handlers;
pub mod validation;
pub mod middleware;