use perseus::prelude::*;
use sycamore::prelude::*;
use serde::{Serialize, Deserialize};
use crate::{
    components::{
        form_text_input::FormTextField,
        form_password_input::FormPasswordField,
        form_button::FormButton,
        layout::Layout,
        form_default::DefaultForm,
    }, request_types::account_requests::{RegisterRequest, RegisterResponse},
};

#[derive(Serialize, Deserialize, Clone, Debug, ReactiveState)]
#[rx(alias = "RegisterDataRx")]
pub struct RegisterData {
    pub first_name: String,
    pub lastname: String,
    pub email: String,
    pub username: String,
    // Do not put password here, it should not be stored!
}

pub fn register_page<'a, G: Html>(cx: BoundedScope<'_, 'a>, register_data: &'a RegisterDataRx) -> View<G> {

    let form_valid = create_signal(cx, true);
    let first_name_valid = create_signal(cx, true);
    let last_name_valid = create_signal(cx, true);
    let email_valid = create_signal(cx, true);
    let username_valid = create_signal(cx, true);
    let password_valid = create_signal(cx, true);

    let register_signal: &Signal<Option<(u16, String)>> = create_signal(cx, None);
    create_effect(cx, || {
        match &(*register_signal.get()) {
            Some(result) => {
                form_valid.set(true);
                if let Ok(result) = serde_json::from_str::<RegisterResponse>(&result.1){
                    first_name_valid.set(result.firstname_ok);
                    last_name_valid.set(result.last_name_ok);
                    email_valid.set(result.email_ok);
                    username_valid.set(result.username_ok);
                    password_valid.set(result.password_ok);
                    
                    if result.email_ok
                    && result.firstname_ok
                    && result.last_name_ok
                    && result.password_ok
                    && result.username_ok {
                        // TODO navigate to 'confirm email' instead of to 'login'
                        navigate("login");
                    }
                } else {
                    form_valid.set(false);
                    username_valid.set(false);
                }
            },
            None => {
                // Initial state
                web_log!("Reset response signal");
                form_valid.set(false);
                first_name_valid.set(true);
                last_name_valid.set(true);
                email_valid.set(true);
                username_valid.set(true);
                password_valid.set(true);
            }
        };
    });

    let password = create_signal(cx, "".to_string());

    let submit_fn = || {

        
        Some(RegisterRequest {
            first_name: (*register_data.first_name.get()).clone(),
            last_name: (*register_data.lastname.get()).clone(),
            email: (*register_data.email.get()).clone(),
            username: (*register_data.username.get()).clone(),
            password: (*password.get()).clone()
        })
        
    };

    

    view! { cx,
        Layout {
            div {
                DefaultForm(label_id="title_register"){
                    FormTextField(value = &register_data.first_name, label_id = "first-name", placeholder_id="first-name", valid=first_name_valid, error_id="error_first_name_invalid")
                    FormTextField(value = &register_data.lastname, label_id = "last-name", placeholder_id="last-name", valid = last_name_valid, error_id="error_last_name_invalid")
                    FormTextField(value = &register_data.email, label_id = "email", placeholder_id="email", valid = email_valid, error_id="email_invalid")
                    FormTextField(value = &register_data.username, label_id = "username", placeholder_id="username", valid = username_valid, error_id="error_username_invalid")
                    FormPasswordField(value = &password, label_id = "password", valid = password_valid, error_id="error_password_invalid")
                    FormButton(
                        label_id="Register",
                        target="/api/register",
                        data=submit_fn,
                        result=register_signal,
                    )
                }
            }
        }
    }
}

#[engine_only_fn]
pub fn head(cx: Scope) -> View<SsrNode> {
    view! { cx,
        title { "title_register" } // TODO Translation
    }
}

pub fn get_template<G: Html>() -> Template<G> {
    Template::
    build("register")
    .build_state_fn(get_register_state)
    .view_with_state(register_page)
    .head(head)
    .build()
}

#[engine_only_fn]
async fn get_register_state(_info: StateGeneratorInfo<()>) -> RegisterData {
    RegisterData {
        first_name: "".to_owned(),
        lastname: "".to_owned(),
        email: "".to_owned(),
        username: "".to_owned(),
    }
}
