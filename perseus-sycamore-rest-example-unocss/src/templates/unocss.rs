use perseus::prelude::*;
use sycamore::prelude::*;

use crate::{components::{
    layout::Layout,
    form_default::DefaultForm,
}};

fn unocss_page<G: Html>(cx: Scope) -> View<G> {
    view! { cx,
        Layout {
            div(style = "") {
                DefaultForm(label_id="Welcome to a Perseus project styled with Unocss + Daisyui"){
                    p {"This project is far from perfect, it is only made to show that it is possible"}
                    p {"There are some issues that are still present"}
                    p {"The biggest issue is, that the following should be used instead of daisyui: unocss-preset-daisy @kidonng/daisyui"}
                    
                    a (class="text-info", href="https://github.com/kidonng/unocss-preset-daisy", rel="external"){"https://github.com/kidonng/unocss-preset-daisy"}
                    p {"Because we don't use tailwind, no purging is currently performed and so the css file is huge."}
                    p {"If we would use @kidonng/daisyui as described on the unocss homepage, no purging would be necessary."}
                    p {"But I did not get it to work correctly."}
                    p {"The main issue is, that the unocss 'transformerDirectives' don't seem to work with rust files" }
                    p {"That means, that the @apply rules in the css files do not get resolved"}
                    p {"The second issue is hosting the whole folder because it is a lot of files and creating that many static_alias is not easy"}
                    p {"Currently it would be neccessary to create a custom api route,"}
                    p {"Or to manually copy the folder 'node_modules/@kidonng/daisyui' into the 'static' folder"}
                    p {"By the way, we use unocss icons and selected the "} a(class="text-info", href="https://icon-sets.iconify.design/material-symbols", rel="external"){ "material-symbols" }
                    p {"Have a look at all this icons, I did not even realize how many free icons there are!"}
                    div (class="text-accent i-material-symbols-emoji-objects-outline-rounded text-3xl")
                    a (class="text-info", href="https://icon-sets.iconify.design/", rel="external"){"https://icon-sets.iconify.design/"}

                }
            }
        }
    }
}

#[engine_only_fn]
fn head(cx: Scope) -> View<SsrNode> {
    view! { cx,
        title { "Welcome to Perseus + Unocss/Daisyui" }
    }
}

pub fn get_template<G: Html>() -> Template<G> {
    Template::build("unocss").view(unocss_page).head(head).build()
}