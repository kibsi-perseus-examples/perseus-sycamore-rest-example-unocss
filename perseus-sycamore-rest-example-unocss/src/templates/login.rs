use perseus::prelude::*;
use serde::{Serialize, Deserialize};
use sycamore::prelude::*;
use crate::{
    components::{
        form_text_input::FormTextField,
        form_password_input::FormPasswordField,
        form_button::FormButton,
        layout::Layout,
        form_default::DefaultForm,
    }, request_types::account_requests::{LoginResponse, LoginRequest},
};

#[derive(Serialize, Deserialize, Clone, ReactiveState)]
#[rx(alias = "LoginDataRx")]
pub struct LoginData {
    pub username: String,
    pub password: String,
    // Helpers
}

pub fn login_page<'a, G: Html>(cx: BoundedScope<'_, 'a>, login_data: &'a LoginDataRx) -> View<G> {
    // Do not put password_visible into the reactive state, it should always be initialized with false
    // Same for the other fields down below

    let username_valid = create_signal(cx, true);
    let password_valid = create_signal(cx, true);

    let login_fn = || Some(LoginRequest{
        username: (*login_data.username.get_untracked()).clone(),
        password: (*login_data.password.get_untracked()).clone(),
    });
    
    let login_signal: &Signal<Option<(u16, String)>> = create_signal(cx, None);

    create_effect(cx, || {
        //let s = &(*register_signal.get());
        match &(*login_signal.get()) {
            Some(result) => {
                let response = serde_json::from_str::<LoginResponse>(&result.1);
                if let Ok(login_response) = response {
                    if !login_response.account_activated {
                        //TODO: Maybe a popup? - This should not happen in the current example unless it is modified
                    } else {
                        navigate("/api/protected")
                    }
                } else {
                    password_valid.set(false);
                }
            }
            None => {
                password_valid.set(false);
            }
        };
    });    

    view! { cx,
        Layout {
            div {
                DefaultForm(label_id="title_login"){
                    FormTextField(value = &login_data.username, label_id = "username", placeholder_id="username", valid = username_valid)
                    FormPasswordField(value = &login_data.password, label_id = "password", valid = password_valid)
                    
                    FormButton(label_id="Login",
                    target="/api/login",
                    data=login_fn,
                    result=login_signal,
                )
                }
            }
        }
    }
}

#[engine_only_fn]
pub fn head(cx: Scope) -> View<SsrNode> {
    view! { cx,
        title { ("title_login") } //TODO Translation
    }
}

pub fn get_template<G: Html>() -> Template<G> {
    Template::
    build("login")
    .build_state_fn(get_login_state)
    .view_with_state(login_page)
    .head(head)
    .build()
}

#[engine_only_fn]
async fn get_login_state(_info: StateGeneratorInfo<()>) -> LoginData {
    LoginData {
        username: "".to_owned(),
        password: "".to_owned(), //TODO: Remove this, we do not want to persist the password, make a signal in the view fn instead
        // Helpers
    }
}