use perseus::prelude::*;
use sycamore::prelude::*;

use crate::{components::{
    layout::Layout,
    form_default::DefaultForm,
}};

fn index_page<G: Html>(cx: Scope) -> View<G> {
    view! { cx,
        Layout {
            div(style = "") {
                DefaultForm(label_id="Welcome to a Perseus project styled with Unocss + Daisyui"){
                    p {"This project is far from perfect, it is only made to show that it is possible"}
                    p {"Have a look at the "} a (class="text-info", href="/unocss") {"Unocss + DaysiUi + icons information"}
                    p {"Check out the"} a (class="text-info",rel="external", href="http://localhost:8080/api/status") {"backend greeting message"}
                    p (class="text-2xl") {"Update:"}
                    p {"A DB was added (Sqlite)"}
                    p {"It is now possible to create an account (Register)"}
                    p {"and to log in"}
                    p {"A custom backend server was implemented with Axum"}
                    p {"REST requests are sent from the browser to the api (Register, Login, Logout, ...)"}
                    p {"There is a lot going on on the backend (Custom Errors, Middleware, Sessions, handlers, DB + DAOs + Entities)"}
                    p {"I maybe find the time in the future to improve this example"}
                    p {"There is still a lot to improve..."}
                    p {"But it takes so much time and I want to work on my actual project (where this all is extracted from)"}
                    p {"Maybe there are errors in this project, I currently don't have the time and energy to test it all (again, I want to work on my actual project)"}
                    p (class="text-2xl") {"If the Sqlite DB is missing:"}
                    p {"If the sqlite DB suddenly is missing, edit the build.rs file in any way. Then save it. That should re-build the DB"}
                    p {"In my actual private project I use CockroachDB because the have a really nice free tier with 5GB storage."}
                    p {"I had a hard time adjusting back from CockroachDB to Sqlite, so the DB Entities and DAOs are far from perfect"}
                    p {"But Sqlite is just a file and so it was perfect for this example. I would not recommend it for production though."}
                    p {"If you think this is already complex, think of all the other things that you still have to do: (DB Migration, Translation, Mobile friendly, TLS, Load Balancing, DB Back Pressure)"}
                    p {"I'm currently experimenting with gRPC which is really cool, maybe I will add it into this example in the future"}
                    p {"Also Daisyui is not working properly with Unocss yet. If you know how to fix it please let me know! Otherwise stick to Tailwindcss Plugin"}
                    p {"Let me know what you would think about this example and what you would like to see next!"}
                    p {"Greetings!"}
                    p (class="text-primary") {"kibsi (Bernhard Kinast)"}
                }
            }
        }
    }
}

#[engine_only_fn]
fn head(cx: Scope) -> View<SsrNode> {
    view! { cx,
        title { "Welcome to Perseus + Unocss/Daisyui" }
    }
}

pub fn get_template<G: Html>() -> Template<G> {
    Template::build("index").view(index_page).head(head).build()
}