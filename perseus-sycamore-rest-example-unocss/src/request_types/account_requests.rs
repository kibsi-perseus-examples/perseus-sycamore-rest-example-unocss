use serde::{Serialize, Deserialize};


#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RegisterRequest {
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub username: String,
    pub password: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RegisterResponse {
    pub firstname_ok: bool,
    pub last_name_ok: bool,
    pub email_ok: bool,
    pub username_ok: bool,
    pub password_ok: bool,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct LogoutRequest {

}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct LogoutResponse {
    pub logged_out: bool
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct LoginRequest {
    pub username: String,
    pub password: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct LoginResponse {
    pub account_activated: bool,
}