mod templates;
mod components;

mod custom_backend;
mod request_types;

use perseus::prelude::*;
use sycamore::prelude::*;
use tracing_subscriber::EnvFilter;


#[perseus::main(custom_backend::custom_server::server)]
pub fn main<G: Html>() -> PerseusApp<G> {
    dotenvy::dotenv().ok();
    tracing_subscriber::fmt()
        .with_target(true)
        .compact()
        .with_env_filter(EnvFilter::from_default_env())
        .with_thread_ids(true)
        .with_thread_names(false)
        .with_line_number(true)
        .log_internal_errors(true)
        .init();

    PerseusApp::new()
    .static_alias("/reset-tailwind.css",                "node_modules/@unocss/reset/tailwind.css")
    .static_alias("/uno.css",                           "dist/static/uno.css")
    .static_alias("/daisyui-full.css",                  "node_modules/daisyui/dist/full.css")
    .static_alias("/daisyui-styled.css",                "node_modules/daisyui/dist/styled.css")
    .static_alias("/daisyui-themes.css",                "node_modules/daisyui/dist/themes.css")
    // Sadly I don't know an elegant way to make static aliases at the moment, so here we go, every static alias from
    // the npm modules @kidonng/daisyui
    .static_alias("/@kidonng/daisyui/full.css","node_modules/@kidonng/daisyui/full.css")
    .static_alias("/@kidonng/daisyui/full.min.css","node_modules/@kidonng/daisyui/full.min.css")
    .static_alias("/@kidonng/daisyui/index.css","node_modules/@kidonng/daisyui/index.css")
    // base
    .static_alias("/@kidonng/daisyui/base/colors.css","node_modules/@kidonng/daisyui/base/colors.css")
    .static_alias("/@kidonng/daisyui/base/general.css","node_modules/@kidonng/daisyui/base/general.css")
    .static_alias("/@kidonng/daisyui/base/index.css","node_modules/@kidonng/daisyui/base/index.css")
    // components
    .static_alias("/@kidonng/daisyui/components/index.css","node_modules/@kidonng/daisyui/components/index.css")
    // components/styled
    .static_alias("/@kidonng/daisyui/components/styled/alert.css","node_modules/@kidonng/daisyui/components/styled/alert.css")
    .static_alias("/@kidonng/daisyui/components/styled/avatar.css","node_modules/@kidonng/daisyui/components/styled/avatar.css")
    .static_alias("/@kidonng/daisyui/components/styled/badge.css","node_modules/@kidonng/daisyui/components/styled/badge.css")
    .static_alias("/@kidonng/daisyui/components/styled/bottom-navigation.css","node_modules/@kidonng/daisyui/components/styled/bottom-navigation.css")
    .static_alias("/@kidonng/daisyui/components/styled/breadcrumbs.css","node_modules/@kidonng/daisyui/components/styled/breadcrumbs.css")
    .static_alias("/@kidonng/daisyui/components/styled/button.css","node_modules/@kidonng/daisyui/components/styled/button.css")
    .static_alias("/@kidonng/daisyui/components/styled/card.css","node_modules/@kidonng/daisyui/components/styled/card.css")
    .static_alias("/@kidonng/daisyui/components/styled/carousel.css","node_modules/@kidonng/daisyui/components/styled/carousel.css")
    .static_alias("/@kidonng/daisyui/components/styled/chat.css","node_modules/@kidonng/daisyui/components/styled/chat.css")
    .static_alias("/@kidonng/daisyui/components/styled/checkbox.css","node_modules/@kidonng/daisyui/components/styled/checkbox.css")
    .static_alias("/@kidonng/daisyui/components/styled/collapse.css","node_modules/@kidonng/daisyui/components/styled/collapse.css")
    .static_alias("/@kidonng/daisyui/components/styled/countdown.css","node_modules/@kidonng/daisyui/components/styled/countdown.css")
    .static_alias("/@kidonng/daisyui/components/styled/divider.css","node_modules/@kidonng/daisyui/components/styled/divider.css")
    .static_alias("/@kidonng/daisyui/components/styled/drawer.css","node_modules/@kidonng/daisyui/components/styled/drawer.css")
    .static_alias("/@kidonng/daisyui/components/styled/dropdown.css","node_modules/@kidonng/daisyui/components/styled/dropdown.css")
    .static_alias("/@kidonng/daisyui/components/styled/file-input.css","node_modules/@kidonng/daisyui/components/styled/file-input.css")
    .static_alias("/@kidonng/daisyui/components/styled/footer.css","node_modules/@kidonng/daisyui/components/styled/footer.css")
    .static_alias("/@kidonng/daisyui/components/styled/form.css","node_modules/@kidonng/daisyui/components/styled/form.css")
    .static_alias("/@kidonng/daisyui/components/styled/hero.css","node_modules/@kidonng/daisyui/components/styled/hero.css")
    .static_alias("/@kidonng/daisyui/components/styled/index.css","node_modules/@kidonng/daisyui/components/styled/index.css")
    .static_alias("/@kidonng/daisyui/components/styled/input.css","node_modules/@kidonng/daisyui/components/styled/input.css")
    .static_alias("/@kidonng/daisyui/components/styled/kbd.css","node_modules/@kidonng/daisyui/components/styled/kbd.css")
    .static_alias("/@kidonng/daisyui/components/styled/link.css","node_modules/@kidonng/daisyui/components/styled/link.css")
    .static_alias("/@kidonng/daisyui/components/styled/mask.css","node_modules/@kidonng/daisyui/components/styled/mask.css")
    .static_alias("/@kidonng/daisyui/components/styled/menu.css","node_modules/@kidonng/daisyui/components/styled/menu.css")
    .static_alias("/@kidonng/daisyui/components/styled/mockup.css","node_modules/@kidonng/daisyui/components/styled/mockup.css")
    .static_alias("/@kidonng/daisyui/components/styled/modal.css","node_modules/@kidonng/daisyui/components/styled/modal.css")
    .static_alias("/@kidonng/daisyui/components/styled/navbar.css","node_modules/@kidonng/daisyui/components/styled/navbar.css")
    .static_alias("/@kidonng/daisyui/components/styled/progress.css","node_modules/@kidonng/daisyui/components/styled/progress.css")
    .static_alias("/@kidonng/daisyui/components/styled/radial-progress.css","node_modules/@kidonng/daisyui/components/styled/radial-progress.css")
    .static_alias("/@kidonng/daisyui/components/styled/radio.css","node_modules/@kidonng/daisyui/components/styled/radio.css")
    .static_alias("/@kidonng/daisyui/components/styled/range.css","node_modules/@kidonng/daisyui/components/styled/range.css")
    .static_alias("/@kidonng/daisyui/components/styled/rating.css","node_modules/@kidonng/daisyui/components/styled/rating.css")
    .static_alias("/@kidonng/daisyui/components/styled/select.css","node_modules/@kidonng/daisyui/components/styled/select.css")
    .static_alias("/@kidonng/daisyui/components/styled/stack.css","node_modules/@kidonng/daisyui/components/styled/stack.css")
    .static_alias("/@kidonng/daisyui/components/styled/stat.css","node_modules/@kidonng/daisyui/components/styled/stat.css")
    .static_alias("/@kidonng/daisyui/components/styled/steps.css","node_modules/@kidonng/daisyui/components/styled/steps.css")
    .static_alias("/@kidonng/daisyui/components/styled/swap.css","node_modules/@kidonng/daisyui/components/styled/swap.css")
    .static_alias("/@kidonng/daisyui/components/styled/tab.css","node_modules/@kidonng/daisyui/components/styled/tab.css")
    .static_alias("/@kidonng/daisyui/components/styled/table.css","node_modules/@kidonng/daisyui/components/styled/table.css")
    .static_alias("/@kidonng/daisyui/components/styled/textarea.css","node_modules/@kidonng/daisyui/components/styled/textarea.css")
    .static_alias("/@kidonng/daisyui/components/styled/toast.css","node_modules/@kidonng/daisyui/components/styled/toast.css")
    .static_alias("/@kidonng/daisyui/components/styled/toggle.css","node_modules/@kidonng/daisyui/components/styled/toggle.css")
    .static_alias("/@kidonng/daisyui/components/styled/tooltip.css","node_modules/@kidonng/daisyui/components/styled/tooltip.css")
    .static_alias("/@kidonng/daisyui/components/styled/typography.css","node_modules/@kidonng/daisyui/components/styled/typography.css")
    // components/unstyled
    .static_alias("/@kidonng/daisyui/components/unstyled/alert.css","node_modules/@kidonng/daisyui/components/unstyled/alert.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/artboard.css","node_modules/@kidonng/daisyui/components/unstyled/artboard.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/avatar.css","node_modules/@kidonng/daisyui/components/unstyled/avatar.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/badge.css","node_modules/@kidonng/daisyui/components/unstyled/badge.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/bottom-navigation.css","node_modules/@kidonng/daisyui/components/unstyled/bottom-navigation.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/breadcrumbs.css","node_modules/@kidonng/daisyui/components/unstyled/breadcrumbs.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/button.css","node_modules/@kidonng/daisyui/components/unstyled/button.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/card.css","node_modules/@kidonng/daisyui/components/unstyled/card.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/carousel.css","node_modules/@kidonng/daisyui/components/unstyled/carousel.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/chat.css","node_modules/@kidonng/daisyui/components/unstyled/chat.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/checkbox.css","node_modules/@kidonng/daisyui/components/unstyled/checkbox.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/collapse.css","node_modules/@kidonng/daisyui/components/unstyled/collapse.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/countdown.css","node_modules/@kidonng/daisyui/components/unstyled/countdown.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/divider.css","node_modules/@kidonng/daisyui/components/unstyled/divider.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/drawer.css","node_modules/@kidonng/daisyui/components/unstyled/drawer.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/dropdown.css","node_modules/@kidonng/daisyui/components/unstyled/dropdown.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/file-input.css","node_modules/@kidonng/daisyui/components/unstyled/file-input.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/footer.css","node_modules/@kidonng/daisyui/components/unstyled/footer.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/form.css","node_modules/@kidonng/daisyui/components/unstyled/form.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/hero.css","node_modules/@kidonng/daisyui/components/unstyled/hero.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/index.css","node_modules/@kidonng/daisyui/components/unstyled/index.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/indicator.css","node_modules/@kidonng/daisyui/components/unstyled/indicator.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/input.css","node_modules/@kidonng/daisyui/components/unstyled/input.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/kbd.css","node_modules/@kidonng/daisyui/components/unstyled/kbd.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/link.css","node_modules/@kidonng/daisyui/components/unstyled/link.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/mask.css","node_modules/@kidonng/daisyui/components/unstyled/mask.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/menu.css","node_modules/@kidonng/daisyui/components/unstyled/menu.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/mockup.css","node_modules/@kidonng/daisyui/components/unstyled/mockup.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/modal.css","node_modules/@kidonng/daisyui/components/unstyled/modal.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/navbar.css","node_modules/@kidonng/daisyui/components/unstyled/navbar.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/progress.css","node_modules/@kidonng/daisyui/components/unstyled/progress.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/radial-progress.css","node_modules/@kidonng/daisyui/components/unstyled/radial-progress.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/radio.css","node_modules/@kidonng/daisyui/components/unstyled/radio.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/range.css","node_modules/@kidonng/daisyui/components/unstyled/range.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/rating.css","node_modules/@kidonng/daisyui/components/unstyled/rating.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/select.css","node_modules/@kidonng/daisyui/components/unstyled/select.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/stack.css","node_modules/@kidonng/daisyui/components/unstyled/stack.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/stat.css","node_modules/@kidonng/daisyui/components/unstyled/stat.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/steps.css","node_modules/@kidonng/daisyui/components/unstyled/steps.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/swap.css","node_modules/@kidonng/daisyui/components/unstyled/swap.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/tab.css","node_modules/@kidonng/daisyui/components/unstyled/tab.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/table.css","node_modules/@kidonng/daisyui/components/unstyled/table.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/textarea.css","node_modules/@kidonng/daisyui/components/unstyled/textarea.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/toast.css","node_modules/@kidonng/daisyui/components/unstyled/toast.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/toggle.css","node_modules/@kidonng/daisyui/components/unstyled/toggle.css")
    .static_alias("/@kidonng/daisyui/components/unstyled/tooltip.css","node_modules/@kidonng/daisyui/components/unstyled/tooltip.css")
    
    // themes
    .static_alias("/@kidonng/daisyui/themes/acid.css","node_modules/@kidonng/daisyui/themes/acid.css")
    .static_alias("/@kidonng/daisyui/themes/aqua.css","node_modules/@kidonng/daisyui/themes/aqua.css")
    .static_alias("/@kidonng/daisyui/themes/auto.css","node_modules/@kidonng/daisyui/themes/auto.css")
    .static_alias("/@kidonng/daisyui/themes/autumn.css","node_modules/@kidonng/daisyui/themes/autumn.css")
    .static_alias("/@kidonng/daisyui/themes/black.css","node_modules/@kidonng/daisyui/themes/black.css")
    .static_alias("/@kidonng/daisyui/themes/bumblebee.css","node_modules/@kidonng/daisyui/themes/bumblebee.css")
    .static_alias("/@kidonng/daisyui/themes/business.css","node_modules/@kidonng/daisyui/themes/business.css")
    .static_alias("/@kidonng/daisyui/themes/cmyk.css","node_modules/@kidonng/daisyui/themes/cmyk.css")
    .static_alias("/@kidonng/daisyui/themes/coffee.css","node_modules/@kidonng/daisyui/themes/coffee.css")
    .static_alias("/@kidonng/daisyui/themes/corporate.css","node_modules/@kidonng/daisyui/themes/corporate.css")
    .static_alias("/@kidonng/daisyui/themes/cupcake.css","node_modules/@kidonng/daisyui/themes/cupcake.css")
    .static_alias("/@kidonng/daisyui/themes/cyberpunk.css","node_modules/@kidonng/daisyui/themes/cyberpunk.css")
    .static_alias("/@kidonng/daisyui/themes/dark.css","node_modules/@kidonng/daisyui/themes/dark.css")
    .static_alias("/@kidonng/daisyui/themes/dracula.css","node_modules/@kidonng/daisyui/themes/dracula.css")
    .static_alias("/@kidonng/daisyui/themes/emerald.css","node_modules/@kidonng/daisyui/themes/emerald.css")
    .static_alias("/@kidonng/daisyui/themes/fantasy.css","node_modules/@kidonng/daisyui/themes/fantasy.css")
    .static_alias("/@kidonng/daisyui/themes/forest.css","node_modules/@kidonng/daisyui/themes/forest.css")
    .static_alias("/@kidonng/daisyui/themes/garden.css","node_modules/@kidonng/daisyui/themes/garden.css")
    .static_alias("/@kidonng/daisyui/themes/halloween.css","node_modules/@kidonng/daisyui/themes/halloween.css")
    .static_alias("/@kidonng/daisyui/themes/index.css","node_modules/@kidonng/daisyui/themes/index.css")
    .static_alias("/@kidonng/daisyui/themes/lemonade.css","node_modules/@kidonng/daisyui/themes/lemonade.css")
    .static_alias("/@kidonng/daisyui/themes/light.css","node_modules/@kidonng/daisyui/themes/light.css")
    .static_alias("/@kidonng/daisyui/themes/lofi.css","node_modules/@kidonng/daisyui/themes/lofi.css")
    .static_alias("/@kidonng/daisyui/themes/luxury.css","node_modules/@kidonng/daisyui/themes/luxury.css")
    .static_alias("/@kidonng/daisyui/themes/night.css","node_modules/@kidonng/daisyui/themes/night.css")
    .static_alias("/@kidonng/daisyui/themes/pastel.css","node_modules/@kidonng/daisyui/themes/pastel.css")
    .static_alias("/@kidonng/daisyui/themes/retro.css","node_modules/@kidonng/daisyui/themes/retro.css")
    .static_alias("/@kidonng/daisyui/themes/synthwave.css","node_modules/@kidonng/daisyui/themes/synthwave.css")
    .static_alias("/@kidonng/daisyui/themes/valentine.css","node_modules/@kidonng/daisyui/themes/valentine.css")
    .static_alias("/@kidonng/daisyui/themes/winter.css","node_modules/@kidonng/daisyui/themes/winter.css")
    .static_alias("/@kidonng/daisyui/themes/wireframe.css","node_modules/@kidonng/daisyui/themes/wireframe.css")


    // utilities/global
    .static_alias("/@kidonng/daisyui/utilities/global/borderRadius.css","node_modules/@kidonng/daisyui/utilities/global/borderRadius.css")
    .static_alias("/@kidonng/daisyui/utilities/global/glass.css","node_modules/@kidonng/daisyui/utilities/global/glass.css")
    .static_alias("/@kidonng/daisyui/utilities/global/index.css","node_modules/@kidonng/daisyui/utilities/global/index.css")
    .static_alias("/@kidonng/daisyui/utilities/global/sizing.css","node_modules/@kidonng/daisyui/utilities/global/sizing.css")
    .static_alias("/@kidonng/daisyui/utilities/global/variables.css","node_modules/@kidonng/daisyui/utilities/global/variables.css")

    // utilities
    .static_alias("/@kidonng/daisyui/utilities/index.css","node_modules/@kidonng/daisyui/utilities/index.css")

    // utilities/styled
    .static_alias("/@kidonng/daisyui/utilities/styled/artboard.css","node_modules/@kidonng/daisyui/utilities/styled/artboard.css")
    .static_alias("/@kidonng/daisyui/utilities/styled/avatar.css","node_modules/@kidonng/daisyui/utilities/styled/avatar.css")
    .static_alias("/@kidonng/daisyui/utilities/styled/button.css","node_modules/@kidonng/daisyui/utilities/styled/button.css")
    .static_alias("/@kidonng/daisyui/utilities/styled/card.css","node_modules/@kidonng/daisyui/utilities/styled/card.css")
    .static_alias("/@kidonng/daisyui/utilities/styled/divider.css","node_modules/@kidonng/daisyui/utilities/styled/divider.css")
    .static_alias("/@kidonng/daisyui/utilities/styled/index.css","node_modules/@kidonng/daisyui/utilities/styled/index.css")
    .static_alias("/@kidonng/daisyui/utilities/styled/menu.css","node_modules/@kidonng/daisyui/utilities/styled/menu.css")
    .static_alias("/@kidonng/daisyui/utilities/styled/modal.css","node_modules/@kidonng/daisyui/utilities/styled/modal.css")
    .static_alias("/@kidonng/daisyui/utilities/styled/stat.css","node_modules/@kidonng/daisyui/utilities/styled/stat.css")
    .static_alias("/@kidonng/daisyui/utilities/styled/steps.css","node_modules/@kidonng/daisyui/utilities/styled/steps.css")
    .static_alias("/@kidonng/daisyui/utilities/styled/table.css","node_modules/@kidonng/daisyui/utilities/styled/table.css")

    // utilities/unstyled
    .static_alias("/@kidonng/daisyui/utilities/unstyled/artboard.css","node_modules/@kidonng/daisyui/utilities/unstyled/artboard.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/badge.css","node_modules/@kidonng/daisyui/utilities/unstyled/badge.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/bottom-navigation.css","node_modules/@kidonng/daisyui/utilities/unstyled/bottom-navigation.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/button.css","node_modules/@kidonng/daisyui/utilities/unstyled/button.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/card.css","node_modules/@kidonng/daisyui/utilities/unstyled/card.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/checkbox.css","node_modules/@kidonng/daisyui/utilities/unstyled/checkbox.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/divider.css","node_modules/@kidonng/daisyui/utilities/unstyled/divider.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/file-input.css","node_modules/@kidonng/daisyui/utilities/unstyled/file-input.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/index.css","node_modules/@kidonng/daisyui/utilities/unstyled/index.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/indicator.css","node_modules/@kidonng/daisyui/utilities/unstyled/indicator.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/input.css","node_modules/@kidonng/daisyui/utilities/unstyled/input.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/kbd.css","node_modules/@kidonng/daisyui/utilities/unstyled/kbd.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/menu.css","node_modules/@kidonng/daisyui/utilities/unstyled/menu.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/modal.css","node_modules/@kidonng/daisyui/utilities/unstyled/modal.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/radio.css","node_modules/@kidonng/daisyui/utilities/unstyled/radio.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/range.css","node_modules/@kidonng/daisyui/utilities/unstyled/range.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/rating.css","node_modules/@kidonng/daisyui/utilities/unstyled/rating.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/select.css","node_modules/@kidonng/daisyui/utilities/unstyled/select.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/stat.css","node_modules/@kidonng/daisyui/utilities/unstyled/stat.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/steps.css","node_modules/@kidonng/daisyui/utilities/unstyled/steps.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/tab.css","node_modules/@kidonng/daisyui/utilities/unstyled/tab.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/textarea.css","node_modules/@kidonng/daisyui/utilities/unstyled/textarea.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/toast.css","node_modules/@kidonng/daisyui/utilities/unstyled/toast.css")
    .static_alias("/@kidonng/daisyui/utilities/unstyled/toggle.css","node_modules/@kidonng/daisyui/utilities/unstyled/toggle.css")

    .template(crate::templates::index::get_template())
    .template(crate::templates::unocss::get_template())
    .template(crate::templates::login::get_template())
    .template(crate::templates::register::get_template())
    .index_view(|cx| {
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // Signals do not work here!!!!!!!!!!
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        view! { cx,
            head {
                meta ( charset="utf-8", )
                meta ( name="viewport", content="width=device-width, initial-scale=1, shrink-to-fit=no")
                // first reset with tailwind compatible reset
                link ( rel="stylesheet", href="/reset-tailwind.css")



                // The following does not work sadly:
                // The @apply and so on which should be transformed by the {transformDirectives} from '@unocss/transformer-directives'
                // are not transformed. I do not understand how that should work, how should the css file magically
                // get different content? How do other frameworks solve that? Vite? Astro? Nuxt?
                // They must have some inbetween step where the css file gets processed before it gets sent to the client
                
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // !!!!!!!!! The page does not get rendered correctly whith this !!!!!!!!!!
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // link ( rel="stylesheet", href="/@kidonng/daisyui/index.css")
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                link ( rel="stylesheet", href="/uno.css")

                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // TODO: We would have to use @kidonng/daisyui instead to work properly with unocss
                // But the transformDirectives do not work
                // Try to switch it out and look at the page!
                // What to do, what to do??
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                link ( rel="stylesheet", href="/daisyui-full.css")
                link ( rel="stylesheet", href="/daisyui-styled.css")
                link ( rel="stylesheet", href="/daisyui-themes.css")
            }
            body {
                PerseusRoot()
            }
        }
    })
}
