use sycamore::prelude::*;

#[derive(Prop)]
pub struct DefaultFormProps<'a,G: Html> {
    #[builder(default)]
    pub label_id: &'static str,
    pub children: Children<'a, G>,
}

#[component]
pub fn DefaultForm<'a, G:Html>(cx: Scope<'a>, props: DefaultFormProps<'a,G>) -> View<G> {
    let children = props.children.call(cx);
    view!{cx,
        div(class="card bg-base-200 text-base-content w-240 !rounded-2xl") {
            h2 (class="card-title bg-primary text-primary-content p-5 rounded-t-2xl") { (props.label_id) }
            div(class="card-body"){
                
                section (class="section is-small control") {
                    (children)
                }
                
            }
        }
    }
}