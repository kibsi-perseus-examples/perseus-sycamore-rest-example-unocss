use sycamore::prelude::*;
// used to select a random theme to show the scenes
use rand::seq::SliceRandom; 

#[component]
pub fn Layout<'a, G: Html>(
    cx: Scope<'a>,
    LayoutProps { children }: LayoutProps<'a, G>,
) -> View<G> {

    let possible_themes = vec![
        "light".to_owned(),
        "dark".to_owned(),
        "cupcake".to_owned(),
        "bumblebee".to_owned(),
        "emerald".to_owned(),
        "corporate".to_owned(),
        "synthwave".to_owned(),
        "retro".to_owned(),
        "cyberpunk".to_owned(),
        "valentine".to_owned(),
        "halloween".to_owned(),
        "garden".to_owned(),
        "forest".to_owned(),
        "aqua".to_owned(),
        "lofi".to_owned(),
        "pastel".to_owned(),
        "fantasy".to_owned(),
        "wireframe".to_owned(),
        "black".to_owned(),
        "luxury".to_owned(),
        "dracula".to_owned(),
        "cmyk".to_owned(),
        "autumn".to_owned(),
        "business".to_owned(),
        "acid".to_owned(),
        "lemonade".to_owned(),
        "night".to_owned(),
        "coffee".to_owned(),
        "winter".to_owned(),
    ];

    let children = children.call(cx);
    // used to select a random theme to show the scenes
    let initial_selected_theme_string = possible_themes.choose(&mut rand::thread_rng()).unwrap().clone();
    let selected_theme = create_signal(cx, initial_selected_theme_string.clone());
    // The initially selected theme as a String
    // Convert the theme strings into options for the dropdown
    let theme_options_view = View::new_fragment(
        possible_themes.into_iter().map(|theme|{
            let is_selected = initial_selected_theme_string.eq(&theme);
            view!{cx,
                option(
                    selected=is_selected
                )
                {(theme)}}
            }
            ).collect()
    );

    view! { cx,
        div (class="drawer bg-base-100 text-base-content") {
            input(id="vino-drawer", type="checkbox", class="drawer-toggle")
            div (class="drawer-content") {
                header(class="min-h-5rem", data-theme=*selected_theme.get()) {
                    div (class="navbar bg-base-200 text-base-content") {
                        div (class="navbar-start") {
                            label (for="vino-drawer", class="btn btn-primary drawer-button") {
                                div ( class="i-material-symbols-menu text-3xl" )
                            }
                            a (class="btn btn-ghost normal-case text-xl", href="/") {"Perseus - Unocss Example"}
                        }
                        div (class="navbar-center") {
                            a (class="btn btn-ghost normal-case text-xl", href="/unocss") {
                                strong { "Unocss Info (In Development)" }
                            }
                        }
                        div (class="navbar-end") {
                            ul (class="menu-horizontal menu !flex-row !inline-flex") {
                                li {
                                    a(class="btn btn-success", href="/login") {
                                        "Login"
                                    }
                                }
                                li {
                                    a(class="btn btn-link", href="/register") {
                                        "Register"
                                    }
                                }
                            }
                           
                            select(class="select w-200px bg-base-100 text-base-content",
                            bind:value=selected_theme
                            ) {
                                (theme_options_view)
                            }
                        }
                    }
                }
                main(data-theme=*selected_theme.get(), class="min-h-90% mt-5rem") {
                    
                    div(class="drawer-content relative overflow-x-auto"){
                        div(class="alert alert-warning shadow-lg") {
                            div {
                                label(class="i-material-symbols-bolt-outline text-3xl")
                                span(){"Warning: Work in progress! + A new Random style is chosen on every page load!"}
                            }
                        }
                        (children)
                    }
                }
                footer(data-theme=*selected_theme.get(), class="footer min-h-5% bg-neutral text-neutral-content") {
                    p { "Hey there, I'm a footer!" }
                }
            }
        
            div(class="drawer-side"){
                label(for="vino-drawer", class="drawer-overlay")
                ul(class="menu p-4 w-80 bg-base-200 text-base-content"){
                    li { a (href="/") { "Home" } }
                    li { a (href="/unocss") { "Unocss Info" } }
                    li { a (href="/login" ) { "Login" } }
                    li { a (href="/register") { "Register" } }
                    li { a (href="http://localhost:8080/api/status", rel="external") { "Api Status" } }
                    li { a (href="http://localhost:8080/api/protected", rel="external") { "Protected" } }
                }
            }
        }
    }
}

#[derive(Prop)]
pub struct LayoutProps<'a, G: Html> {
    /// The content to put inside the layout.
    pub children: Children<'a, G>,
}