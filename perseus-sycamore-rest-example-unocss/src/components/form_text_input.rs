use sycamore::prelude::*;
use perseus::prelude::*;
// Try to use capsules instead, because of caching and reusing!

#[derive(Prop)]
pub struct FormTextFieldProps<'a> {
    pub value: &'a Signal<String>,
    #[builder(default)]
    pub label_id: &'static str,
    #[builder(default)]
    pub error_id: &'static str,
    #[builder(default)]
    pub placeholder_id: &'static str,
    pub valid: &'a Signal<bool>,
}


#[component]
pub fn FormTextField<'a, G:Html>(cx: Scope<'a>, props: FormTextFieldProps<'a>) -> View<G> {
    let input_class = create_memo(cx, || if *props.valid.get() {"input"} else {"input is-danger"});
    let error_visible = create_selector(cx, ||!*props.valid.get() && !props.error_id.is_empty() && !(*props.value.get()).is_empty());
    view!{cx,
        div (class="field") {
            label (class="label") { (props.label_id) } //TODO Translation
            div (class="control") {
                input (class=input_class,
                    type="input",
                    placeholder=(props.placeholder_id), //TODO Translation
                    bind:value=props.value
                )
            }
        }
        (if *error_visible.get() {
            view! { cx,
                div (class="notification is-danger") {
                    (props.error_id) // TODO Translation
                }
            }
        } else {
            view! { cx, }
        })
    }
}