use serde::Serialize;
use sycamore::prelude::*;
use perseus::prelude::*;

#[derive(Prop)]
pub struct FormButtonProps<'a, T, D>
where T: Fn() -> Option<D>,
D: Serialize,
{
    #[builder(default)]
    pub label_id: &'static str,
    #[builder(default)]
    pub target: &'static str,
    pub data: T,
    pub result: &'a Signal<Option<(u16, String)>>,
}

#[component]
pub fn FormButton<'a, G:Html, T, D>(cx: Scope<'a>, mut props: FormButtonProps<'a, T, D>) -> View<G>
where T: Fn() -> Option<D> +'a,
D: Serialize,
{
    let busy = create_signal(cx, false);
    let button_class = create_memo(cx, || ["btn btn-primary", if *busy.get() {" loading"} else {""}].concat() );

    view!{cx,
        div (class="field") {
            button(class=(*button_class.get()),
                on:click= move |_| click(cx, props.target, &props.data, &props.result, busy)
            ) {
                (props.label_id) //TODO: Add translation instead
            }
        }
    }
}

fn click<'a, D: Serialize,T>(cx: Scope<'a>, target: &'a str, f: T, result_signal: &'a Signal<Option<(u16,String)>>, busy_signal: &'a Signal<bool>)
where T: Fn() -> Option<D>,
{
    let data = f();

    if let Some(data) = data {
        /*let serialized = serde_json::to_string(&data);
        if let Ok(serialized) = serialized {
            web_log!("sending data: {}",serialized);
        }*/
        #[cfg(client)]
        let json_body = serde_json::to_string(&data);
        drop(cx); // I don't remember why this is necessary
        busy_signal.set(true); // Makes the button spin during the request
        result_signal.track(); // Track it just in case, I do not completely understand if it is neccessary here
        #[cfg(client)]
        spawn_local_scoped(cx, async move {
            if let Ok(json_body) = json_body {
                let result = reqwasm::http::Request::post(target)
                .header("Content-Type", "application/json")
                .body(&json_body)
                .send().await;
                let result_string:(u16, String) = match result {
                    Ok(response) => 
                        (response.status(),
                            match response.text().await {
                                Ok(parsed) => parsed,
                                Err(err) => err.to_string()
                            }
                        ),
                    Err(err) => (0, err.to_string()),
                };
                result_signal.set(Some(result_string));
            }
            busy_signal.set(false);
        });
    } else {
        web_log!("Data not present!");
    }
}