use sycamore::prelude::*;
use perseus::prelude::*;
// Try to use capsules instead, because of caching and reusing!

#[derive(Prop)]
pub struct FormPasswordFieldProps<'a> {
    pub value: &'a Signal<String>,
    #[builder(default)]
    pub label_id: &'static str,
    #[builder(default)]
    pub error_id: &'static str,
    pub valid: &'a Signal<bool>,
}


#[component]
pub fn FormPasswordField<'a, G:Html>(cx: Scope<'a>, props: FormPasswordFieldProps<'a>) -> View<G> {
    let input_class = create_memo(cx, || if *props.valid.get() {"input"} else {"input is-danger"});
    let error_visible = create_selector(cx, ||!*props.valid.get() && !props.error_id.is_empty() && !(*props.value.get()).is_empty());
    let password_visible = create_signal(cx, false);
    let password_visible_type = create_memo(cx, || if *password_visible.get() { "input".to_owned()        } else { "password".to_owned() });
    let password_icon         = create_memo(cx, || if *password_visible.get() { "fa-eye-slash".to_owned() } else { "fa-eye".to_owned()   });
    let password_icon_classes = create_memo(cx, || {
        let mut fa_base = "fas ".to_owned();
        fa_base.push_str(password_icon.get().as_str());
        fa_base
    });

    view!{cx,
        div (class="field") {
            label (class="label") { (props.label_id)} // TODO Translation
            div (class="control has-icons-left has-icons-right") {
                input (class=input_class,
                    type=password_visible_type,
                    placeholder="********",
                    bind:value=props.value,
                )
                span (class="icon is-left is-clickable") {
                    i (class=*password_icon_classes.get(),
                        on:click=|_|{ password_visible.set(!*password_visible.get_untracked()) } )
                }
            }
            (if *error_visible.get() {
                view! { cx,
                    div (class="notification is-danger") {
                        (props.error_id) //TODO Translation
                    }
                }
            } else {
                view! { cx, }
            })
        }
    }
}