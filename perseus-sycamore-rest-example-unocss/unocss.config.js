import {
    presetUno,
    defineConfig,
    presetIcons,
    presetWebFonts,
    transformerDirectives,
  } from 'unocss'
import {presetDaisy} from 'unocss-preset-daisy'
//import {transformDirectives} from '@unocss/transformer-directives'

export default defineConfig({
  theme: {
    // ...
    colors: {
    }
  },
  shortcuts: [
    { box: 'max-w-7xl mx-auto bg-gray-100 rounded-md shadow-sm p-4' },
    { 'btn-primary-normal': 'bg-theme-normal-accent text-theme-normal' },
    { 'btn-secondary-normal': 'bg-stone-100 text-slate-900' } //,
    //[ /^bg-theme-normal-(\d+)$/, ([, c]) => `bg-stone-${c}` ],
    //[ /^bg-theme-normal-accent-(\d+)$/, ([, c]) => `bg-stone-${c}` ],
    //[ /^bg-theme-normal-highlight-(\d+)$/, ([, c]) => `bg-rose-${c}` ]
  ],
  transformers: [
    transformerDirectives(),
  ],
  presets: [
    presetUno(),
    presetDaisy(),
    presetIcons(),
    presetWebFonts({
      provider: "google",
      fonts: {
        sans: "Roboto",
      },
    }),
  ],
  preflights: [
    {
      getCSS: ({theme}) => `
      // Some default css
      `
    }
  ]
})