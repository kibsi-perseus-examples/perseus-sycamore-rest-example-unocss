#[cfg(not(engine))]
use std::{io, fs, path::Path};
use sqlx::{migrate::MigrateDatabase, Sqlite, SqlitePool};

const DB_URL: &str = "sqlite://dist/mutable/sqlite.db";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {

    #[cfg(not(engine))]
    build_unocss();

    #[cfg(engine)]
    init_db().await;
    // This would be a way to copy a folder to the dist directory
    //#[cfg(not(engine))]
    //_copy_dir_all("./node_modules/@kidonng/daisyui","./dist/mutable/daisyui")?;
    Ok(())
}

#[cfg(not(engine))]
fn build_unocss() {
    use std::process::Command;
    use execute::Execute;

    const UNOCSS_PATH: &str = "node_modules/@unocss/cli/bin/unocss.mjs";
    // unocss build css file
    // See https://github.com/unocss/unocss/tree/main/packages/cli
    // Install with npm i -D unocss
    // Install cli with npm i -D @unocss/cli
    // test command: ./node_modules/@unocss/cli/bin/unocss.mjs src/**/*.rs -c unocss.config.js -o dist/static/uno.css
    // The file will be in dist/static
    // A static alis was created in main.rs
    let mut command = Command::new(UNOCSS_PATH);
    command.arg("src/**/*.rs");
    command.arg("src/**/*.css");
    command.arg("-c").arg("unocss.config.js");
    command.arg("-o").arg("dist/static/uno.css");
    if let Some(exit_code) = command.execute().unwrap() {
        if exit_code == 0 {
            println!("UNOCSS Ok.");
        } else {
            eprintln!("UNOCSS Failed.");
            match command.output(){
                Ok(output) => panic!("Something is wrong with unocss: {:?}",output),
                Err(err) => panic!("Something is wrong with unocss: {err}")
            }   
        }
    } else {
        eprintln!("UNOCSS Interrupted!");
    }
}

// This would be a way to copy a folder
// copy all files but only for client, not engine
#[cfg(not(engine))]
fn _copy_dir_all(src: impl AsRef<Path>, dst: impl AsRef<Path>) -> io::Result<()> {
    fs::create_dir_all(&dst)?;
    for entry in fs::read_dir(src)? {
        let entry = entry?;
        let ty = entry.file_type()?;
        if ty.is_dir() {
            _copy_dir_all(entry.path(), dst.as_ref().join(entry.file_name()))?;
        } else {
            fs::copy(entry.path(), dst.as_ref().join(entry.file_name()))?;
        }
    }
    Ok(())
}

#[cfg(engine)]
pub async fn init_db() {
    if !Sqlite::database_exists(DB_URL).await.unwrap_or(false) {
        println!("Creating database {}", DB_URL);
        match Sqlite::create_database(DB_URL).await {
            Ok(_) => {
                println!("Create db success")
        
        },
            Err(error) => panic!("error: {}", error),
        }
    } else {
        println!("Database already exists");
    }
    let db = SqlitePool::connect(DB_URL).await.unwrap();

    let user_account_create_table_result = sqlx::query(
r#"
create table if not exists USER_ACCOUNT (
  user_account_id integer primary key autoincrement
, first_name text
, last_name text
);
    "#
    ).execute(&db).await.unwrap();
    println!("Create USER_ACCOUNT table result: {:?}", user_account_create_table_result);


    let user_login_data_create_table_result = sqlx::query(
r#"
create table if not exists USER_LOGIN_DATA (
  user_account_id_ref integer primary key not null
, username text
, password_encrypted text
, email text
, account_activated bool not null
);
    "#
    ).execute(&db).await.unwrap();
    println!("Create USER_LOGIN_DATA table result: {:?}", user_login_data_create_table_result);

    let session_token_create_table_result = sqlx::query(
r#"
create table if not exists SESSION_TOKEN (
  user_account_id_ref integer primary key
, session_token text
, creation_time integer not null
);
    "#
    ).execute(&db).await.unwrap();
    println!("Create session_token table result: {:?}", session_token_create_table_result);


}
